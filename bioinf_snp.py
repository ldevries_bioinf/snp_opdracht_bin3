#!/usr/bin/env python3
"""
I did choose a protein from the pevsner book: neuroglobin
        usage: bioinf_snp.p -n [snp] -p [position] -msa [msa]
"""

__author__ = "Linda de Vries"
__version__ = "9_2020"

import sys
import argparse
import math

CODON_DICT = {
    'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
    'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
    'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
    'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
    'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
    'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
    'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
    'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
    'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
    'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
    'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
    'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
    'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
    'TAC': 'Y', 'TAT': 'Y', 'TAA': '_', 'TAG': '_',
    'TGC': 'C', 'TGT': 'C', 'TGA': '_', 'TGG': 'W'}


def read_file(file):
    """
    This function reads the file and returns the content
    :param file: opens the nucleotide sequence of the protein
    :return: the file content
    """
    file_output = ""
    # reading the file in
    with open(file, "r") as file_obj:
        for lines in file_obj:
            lines = lines.strip()
            if lines.startswith(">"):
                header = lines
            else:
                file_output += lines
    return header, file_output


def find_start(seq):
    """
    This function finds the start codon in the sequence.
    :param seq: the seuence of the chosen protein.
    :return: returns the start codon location
    """
    for nuc in range(len(seq)):
        temp = seq[nuc:nuc+3]
        if temp == "ATG":
            return nuc
    print("there is no start codon in this sequence")
    return 0


def find_stop(seq, start):
    """
    This function finds the stop codon in the sequence.
    :param seq: the seuence of the chosen protein.
    :param start: the start codon location
    :return: returns the stop codon location
    """
    for nuc in range(start, len(seq), 3):
        if nuc in ("TAA", "TAG", "TGA"):
            return nuc
    print("There is no stop codon in this sequence so its probably a clean data.")
    return len(seq)-2


def aa_pos(sequence, pos, snp):
    """
    This function give the amino acid position
    :param sequence: the sequence file
    :param pos: the user given position
    :param snp: the user given single nucleotide polymorphism
    :return: returns the original amino acid and the new amino acid
    """
    seq = sequence[1]
    amin_pos = seq[pos - (pos % 3): pos + (3 - (pos % 3))]
    new_aa_pos = list(amin_pos)
    new_aa_pos[(pos % 3)] = snp
    new_aa_pos = "".join(new_aa_pos)
    amin_acid = CODON_DICT[amin_pos]
    modified_aa = CODON_DICT[new_aa_pos]
    return amin_acid, modified_aa


def aa_mutation(old, new):
    """
    This function looks if it equals or not
    :param old: original amino acid
    :param new: and the new amino acid
    :return: returns the boolean if it equals or not
    """
    return bool(old != new)


def read_msa(msa_file):
    """
    this function reads the multiple sequence alignment file in.
    :param msa_file: the multiple sequence alignment file
    :return: returns the line with the score
    """
    score = ""
    with open(msa_file, "r") as file_obj:
        for lines in file_obj:
            if lines.startswith(" "):
                score += lines[20:].replace("\n", "")
    return score


def consv_crit(pos, msa):
    """
    This function gives the conservation criterion of the multiple sequence alignment
    :param pos: the position in the alignment
    :param msa: the multiple sequence alignment
    :return: returns the position in the multiple sequence alignment
    """
    if msa[pos] == "*":
        print("severe: the mutation is in a highly conserved domain.")
    elif msa[pos] == ":":
        print("potentially severe: Mutation in mildly conserved domain.")
    elif msa[pos] == ".":
        print("unlikely severe: mutation  in barely conserved domain.")
    elif msa[pos] == " ":
        print("inconsequential: mutation in unconserved domain.")


def arguments():
    """
    This function creates an 'argparse' parser and adds arguments.
    :return: the parser arguments
    """
    # Create parser with a description and usage
    parser = argparse.ArgumentParser(
        description='Determines the conservation of the SNP.',
        usage='bioinf_snp.py -n [snp] -p [gene position] -m [msa]'
    )

    # Add the arguments:
    parser.add_argument('-n', '--snp',
                        type=str,
                        metavar='',
                        required=True,
                        help='a SNP Nucleotide it is not case sensitive.')
    parser.add_argument('-p', '--position',
                        type=int,
                        metavar='',
                        required=True,
                        help='give a position in the sequence')
    # Added an default but the assignment says given -m
    parser.add_argument('-m', '--msa',
                        default="data/msa.clustal.txt",
                        type=str,
                        metavar='',
                        required=True,
                        help='Multiple sequence alignment file')
    return parser.parse_args()


def main(args):
    """
    This is the main functions the whole script will be runned by this function.
    :param args: are the arguments from the argparser function.
    :return: returns the whole working script
    """
    sequence = read_file("data/neuroglobin/sequence.fasta")
    snp = str(args.snp).upper()
    position = args.position
    msa_file = args.msa
    msa = read_msa(msa_file)
    start_codon = find_start(sequence[1])
    stop_codon = find_stop(sequence[1], start_codon)

    if snp not in "AGTC":
        print("The given SNP is not an DNA nucleotide. Please run again.")
    elif position < start_codon:
        print("synonymous: the given SNP is located in front of the start codon.")
    elif position >= stop_codon:
        print("synonymous: the given SNP is located after the stop codon")
    elif start_codon <= position <= start_codon + 2:
        print("nonsynonymous (10): the given SNP changed the start codon")
    elif stop_codon <= position <= stop_codon + 2:
        print("nonsynonymous (10): the given SNP changed the stop codon")

    original_aa, new_aa = (aa_pos(sequence, position, snp))
    if aa_mutation(original_aa, new_aa):
        amino_pos = math.ceil(position / 3 - 1)
        consv_crit(amino_pos, msa)
    else:
        print("The mutation has no effect.")
    return 0


if __name__ == '__main__':
    arg = arguments()
    sys.exit(main(arguments()))
